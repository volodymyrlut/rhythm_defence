var mobs = new Array();
var sound = new Array();
var mob_types = new Array();

function mob_type(name, hp, damage, speed, img_src) {
  this.name = name;
  this.hp = hp; 
  this.damage = damage;
  this.speed = speed;
  this.img = new Image();
  this.img.src = img_src;
}

mob_types[0] = new mob_type('Квадрат', 100, 1, 99, 'mob.png');
mob_types[1] = new mob_type('Квадрат', 100, 1, 99, 'mob1.png');
mob_types[2] = new mob_type('Квадрат', 100, 1, 99, 'mob2.png');



	
function Mob(type) {
  canvas = document.createElement('img');
  this.dom = document.getElementById('gamewrapper').appendChild(canvas);
  this.dom.classList.add('mob');
  this.dom.width = 100/3;
  this.dom.height = 100/3;
  this.dom.style.top = "0px";
  this.dom.style.left = "0px";

  this.dom.src = mob_types[type].img.src;
		this.hp = mob_types[type].hp;
  var _this = this;
  this.type = type;
  this.x = this.y = 0;
  this.direction = 0;
  /**
   * Directions: 
   * 0 -right
   * 1 - down
   * 2 - left
   * 3 - up
   */
	
	this.animatego = function(delx, dely) {
	_this.iii=0;
	_this.a = setInterval(function () {
	_this.iii++;
		_this.dom.style.top = (parseFloat(_this.dom.style.top.slice(0, -2)) + delx*1/3)+'px';
		_this.dom.style.left = (parseFloat(_this.dom.style.left.slice(0, -2)) + dely*1/3)+'px';
		if(_this.iii >= 100) {
			clearInterval(_this.a);
			_this.go();
		}
		},100-mob_types[_this.type].speed);
	}
	this.go = function() {
    if (_this.y == map.length - 1 && _this.x == map[_this.y].length - 1) {
		console.log(PlayerHP);
		PlayerHP=PlayerHP - mob_types[_this.type].damage; 
		console.log(PlayerHP);

		if (PlayerHP==0) {
			alert("Game over!!!");
			window.location.href = window.location.href;
		}
		return;
		//todo: exit map and damage player
    }
    _this.setup_direction(); 
    switch (_this.direction) {
      case 0:
		_this.animatego(1,0);
		_this.x++;
        break;
      case 1:		
		_this.animatego(0,1);
		_this.y++;
		break;
      case 2:
		_this.animatego(-1,0);
		_this.x--;
        break;
      case 3: 
		_this.animatego(0,-1);
		_this.y--;
        break;
    }
    //console.log(_this.id + "-------" + _this.x+"-----"+ _this.y);
	//xx = _this.y;
	//xy = _this.x;
		
  };

  this.setup_direction = function() {
    if (this.check_direction()) {
      return;
    }
    switch (this.direction) {
      case 0: 
        this.direction = 1;
        if (this.check_direction()) {
          return;
        }
        this.direction = 3;
        if (this.check_direction()) {
          return;
        }
        this.direction = 2;
        return;
      case 1:
        this.direction = 0;
        if (this.check_direction()) {
          return;
        }
        this.direction = 2;
        if (this.check_direction()) {
          return;
        }
        this.direction = 3;
        return;
      case 2:
        this.direction = 1;   
        if (this.check_direction()) {
          return;
        }
        this.direction = 3;
        if (this.check_direction()) {
          return;
        }
        this.direction = 0;
      case 3:
        this.direction = 0;
        if (this.check_direction()) {
          return;
        }
        this.direction = 2;
        if (this.check_direction()) {
          return;
        }
        this.direction = 1;
        return;
      default:
        throw "IDK where to go. Kill me please";
    }
  };

  /**
   * True if ok, else false
   */
  this.check_direction = function() {
    switch (this.direction) {
      case 0:
        return !(map[this.y].length -1 == this.x || map[this.y][this.x + 1] == 0)
      case 1:
        return !(map.length -1 == this.y || map[this.y + 1][this.x] == 0)
      case 2:
        return !(0 == this.x || map[this.y][this.x - 1] == 0)
      case 3:
        return !(0 == this.y || map[this.y - 1][this.x] == 0)
    }
  };

	this.go();
}




