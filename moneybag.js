/**
 * I know that this class does not
 * need its methods. But I'm not JS, but Java developer,
 * so I code in Java style
 */
 var PlayerHP = 100000;
function Moneybag(initCash) { 
  this.cash = init_cash;

  /**
   * Add cash to moneybag
   */
  this.earn = function(cash) {
    this.cash += cash;
  }

  /**
   * Spend cash from moneybag
   */
  this.spend = function(cash) {
    this.cash -= cash;
  }
  
  /**
   * get number of money in the moneybag
   */
  this.currentCash = function() {
    return this.cash;
  } 
}
