var renderObj = function(obj){
	var clean = document.getElementById('description-object-image').innerHTML='';
	clean = document.getElementById('description-button-group').innerHTML='';
	clean = document.getElementById('description-text-group').innerHTML='';
	for (var key in obj){
        if (obj.hasOwnProperty(key)) {
          if (key == "img"){
          	var image = document.createElement('img');
			this.dom = document.getElementById('description-object-image').appendChild(image);
			this.dom.classList.add('description-image');
			this.dom.src = obj[key];
			} else if (key == 'buttons'){
           	for(var i = 0; i < obj[key].length; i++){
           		var button = document.createElement('div');
				this.dom = document.getElementById('description-button-group').appendChild(button);
				this.dom.classList.add(obj[key][i].name);
				this.dom.innerHTML = obj[key][i].text;
				this.dom.addEventListener("click", obj[key][i].callback);			
           	}
          } else if (key == 'text'){
           	for(var i = 0; i < obj[key].length; i++){
           		var text = document.createElement('div');
				this.dom = document.getElementById('description-text-group').appendChild(text);
				this.dom.classList.add(obj[key][i].property);
				this.dom.innerHTML =obj[key][i].property +" "+ obj[key][i].value;		
				document.getElementById('panel').classList.add('active');
           	}
          }
		}
    }
};