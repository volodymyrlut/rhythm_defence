var towers = [];

function Tower(type, x, y) {
	this.rank = 1;
	this.x = x;
	var _this = this;
	this.y = y;
	this.type = type;
	this.image = "tower_" + this.type + ".png";
	this.damage = this.type * 10 * this.rank * 1.2;
	this.range = this.type * 2 * this.rank * 1.2;
	this.curTarget;


	this.update = function() 
   {
      if (_this.curTarget != null) //если переменная текущей цели не пустая
      {
	   this.mobx = (parseFloat(_this.curTarget.dom.style.top.slice(0, -2)));
	  this.moby = (parseFloat(_this.curTarget.dom.style.left.slice(0, -2)));
         distance = Math.sqrt((_this.x-this.mobx*0.03)*(_this.x-this.mobx*0.03)+(_this.y-this.mobx*0.03)*(_this.y-this.moby*0.03)); //меряем дистанцию до нее
       		console.log("DISTANCE: "+ distance + " RANGE "+_this.range);
	   if (distance < _this.range) //если меньше дистанции поражения пушки
         {
            _this.shoot();
         }
      }
      //comment for commit
      else //иначе
      {
         _this.curTarget = this.SortTargets(); //сортируем цели и получаем новую
      }
	  
   };

 this.SortTargets = function()  {
      _this.closestMobDistance = 0; //инициализация переменной для проверки дистанции до моба
      _this.nearestmob = null; //инициализация переменной ближайшего моба
      _this.sortingMobs = mobs; //находим всех мобов с тегом Monster и создаём массив для сортировки

      _this.sortingMobs.forEach(function(everyTarget)//для каждого моба в массиве
      {
	  this.mobx = (parseFloat(everyTarget.dom.style.top.slice(0, -2)));
	  this.moby = (parseFloat(everyTarget.dom.style.left.slice(0, -2)));
         //если дистанция до моба меньше, чем closestMobDistance или равна нулю
         if (  (Math.sqrt((_this.x-this.mobx*0.03)*(_this.x-this.mobx*0.03)+(_this.y-this.mobx*0.03)*(_this.y-this.moby*0.03))   <  _this.closestMobDistance) || ( _this.closestMobDistance == 0))
         {
            _this.closestMobDistance = Math.sqrt((_this.x-this.mobx*0.03)*(_this.x-this.mobx*0.03)+(_this.y-this.mobx*0.03)*(_this.y-this.moby*0.03)); //Меряем дистанцию от моба до пушки, записываем её в переменную
            _this.nearestmob = everyTarget;//устанавливаем его как ближайшего
         }
      });
      console.log("NEAREST MOB: "+_this.nearestmob);
	  return _this.nearestmob;
     };
   
   
   
   
   this.shoot = function () {
   		console.log("shooting");
		canvas = document.createElement('img');
		this.dom = document.getElementById('gamewrapper').appendChild(canvas);
		this.dom.classList.add('mob'); 
		this.dom.src = 'bullet.png';
		this.dom.style.top = _this.y*100/3+"px";
		this.dom.style.left = _this.x*100/3+"px";
		_this.bullet_y = this.dom.style.top;
		_this.bullet_x = this.dom.style.left;
		_this.curTarget.hp -=_this.damage; 
   if (_this.curTarget.hp<=0) {
   		_this.curTarget.dom.style.display = "none";
   }
  		_this.mobx = (parseFloat(_this.curTarget.dom.style.top.slice(0, -2)));
  		_this.moby = (parseFloat(_this.curTarget.dom.style.left.slice(0, -2)));
  		this.shotDistance = _this.range; //Меряем дистанцию от моба до пушки, записываем её в переменную
 //_this.ikii=0;

	_this.delx=(_this.mobbx-_this.bullet_x)/20;
		_this.dely=(_this.mobby-_this.bullet_y)/20;
	_this.a = setInterval(function () {
	//_this.ikii++;
		_this.bullet_y = (parseFloat(_this.bullet_y.slice(0, -2)) + _this.dely)+'px';
		_this.bullet_x = (parseFloat(_this.bullet_x.slice(0, -2)) + _this.delx)+'px';
		_this.dom.style.top = _this.bullet_y;
		_this.dom.style.left = _this.bullet_x;
		// if(_this.ikii >= 20) {
		// 	clearInterval(_this.a);
		// }
	}, 50);
};


	this.upgrade = function() {
		rank++;
	};
	this.destroy = function() {
		towers.splice(towers.indexOf(this), 1)
	};
	this.outputData = {
		'img': this.image,
		'buttons': [{
			'name': 'upgrade',
			'text': '<div class="panel-close">upgrade</div>',
			'callback': function() {
				console.log("upgrade");
			}
		}, {
			'name': 'delete',
			'text': '<div class="panel-close">delete</div>',
			'callback': function() {
				console.log("delete");
			}
		}],
		'text': [{
			'property': 'rank',
			'value': this.rank
		}, {
			'property': 'type',
			'value': this.type
		}, {
			'property': 'range',
			'value': this.range
		}, {
			'property': 'damage',
			'value': this.damage
		}]
	};
}

//global variables
var element = document.getElementById("game");
var ctx = element.getContext('2d');
console.log("ELEMENT: " + element);
var game_rectangle = element.getBoundingClientRect();
var tower = new Image(); // Создание нового объекта изображения
tower.src = 'tower_1.png';
//global

var clickHandler = function(event) {

	var click_x = (event.x - game_rectangle.left) / 33.333333;
	var click_y = (event.y - game_rectangle.top) / 33.3333333;
	// console.log('click_x - '+click_x);
	// console.log('click_y - '+click_y);
	// console.log('event.x - '+event.x);
	// console.log('event.y - '+event.y);
	// console.log('element.offsetLeft - '+element.offsetLeft);
	// console.log('element.offsetTop - '+element.offsetTop);
	var click_x = (event.x - game_rectangle.left) * 0.03;
	var click_y = (event.y - game_rectangle.top) * 0.03;
/*	console.log('click_x - ' + click_x);
	console.log('click_y - ' + click_y);
	console.log('event.x - ' + event.x);
	console.log('event.y - ' + event.y);
	console.log('element.offsetLeft - ' + element.offsetLeft);
	console.log('element.offsetTop - ' + element.offsetTop);*/


	var x = Math.floor(click_x);
	var y = Math.floor(click_y);
	console.log(map[x][y]);
	var field_available = false;
	if (map[x][y] == 0) {
		var field_available = true
	}
	if (field_available) {
		if (towers.length == 0) {
			var twr = new Tower(1, x, y);
			towers.push(twr);
			drawTower(x, y);
			loadShooting();
			loop();
		} else {
			var free_field = true
			for (var i = 0; i < towers.length; i++) {
				if ((towers[i].x == x) && (towers[i].y == y)) {
					free_field = false;
				}
			}
			if (free_field) {
				var twr = new Tower(1, x, y);
				towers.push(twr);
				drawTower(x, y);
				loadShooting();
			} else { //handling activities click
				for (var i = 0; i < towers.length; i++) {
					if ((towers[i].x == x) && (towers[i].y == y)) {
						renderObj(towers[i].outputData);
					}
				}
			}
		}
	}
}
var drawTower = function(x, y) {
	ctx.drawImage(tower, grid_size * x, grid_size * y, grid_size, grid_size);
}


document.getElementById('game').onclick = clickHandler;


	var loadShooting = function()  {

	for (l_i=0;l_i<towers.length;l_i++) {
	towers[l_i].curTarget=towers[l_i].SortTargets();
	towers[l_i].update();

	}
	}

	var loop = function(){
		setInterval(function() { loadShooting(); }, 500);
	}