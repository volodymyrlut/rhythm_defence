var field = [];
var mob_start = 0;
var map;
var grid_size = 10;

var generateMap = function(size) {
	var field = [];
	var size_x = size*2;
	var size_y = size*1;

  //fill with zeros:
  for(var x = 0; x < size_x; x++){
		field[x] = [];
		for(var y = 0; y < size_y; y++){
			field[x][y] = 0;
		}
	}

/*
  //enterance is always road
	field[0][0] = 1;
	var flag_x = 0;
	var flag_y = 0;

	while ((flag_x < size_x) && (flag_y < size_y)){
		if (field[flag_x][flag_y] == 1){
			if ((flag_x < size_x - 1) && (flag_y < size_y - 1)){
				var option = Math.round(Math.random() + 1);
				if(option == 1){
					flag_x++;
				} else {
					flag_y++;
				}
			} else if (flag_x == size_x-1){
				flag_y++;
			} else if (flag_y == size_y-1){
				flag_x++;
			}

		}
		if ((flag_x < size_x)&&(flag_y < size_y)){
			field[flag_x][flag_y] = 1;
		}
	}
*/

  /*
   * !!! CAUTION, MAGIC NUMBERS ARE HERE !!!
   */
  head = Math.floor(Math.random() * 3) + 2;
  tail = Math.floor(Math.random() * 3) + 2;
  if (Math.floor(Math.random() * 2) == 0) {
    generate_path(field, 0, 0, Math.floor(size_x /2), Math.floor(size_y / 6) + 1); 
    generate_path(field, Math.floor(size_x /2) - 1, Math.floor(size_y / 6), size_x - tail, Math.floor(size_y / 3)); 
    generate_path(field, head, Math.floor(size_y / 2) + 1, size_x, size_y);
    field[size_x - tail - 1][Math.floor(size_y / 3)] = 1;
    field[head][Math.floor(size_y / 2)] = 1;
    for (var i = head; i < size_x - tail; i++) {
      field[i][Math.floor(size_y / 3) + 1] = 1;
    }
  } else {
    generate_path(field, 0, 0, size_x, size_y);
  }
 
  draw_map(size_x, size_y, field);
  map = field;
  mob_start = 1;
}

function generate_path(field, init_x, init_y, dest_x, dest_y) {
  field[init_x][init_y] = 1;
  var flag_x = init_x;
  var flag_y = init_y;
   
  while ((flag_x < dest_x) && (flag_y < dest_y)){
    if (field[flag_x][flag_y] == 1){
      if ((flag_x < dest_x - 1) && (flag_y < dest_y - 1)){
        var option = Math.round(Math.random() + 1);
        if(option == 1){
          flag_x++;
        } else {
          flag_y++;
        } 
      } else if (flag_x == dest_x-1){
        flag_y++;
      } else if (flag_y == dest_y-1){
        flag_x++;
      }      
    }
    if ((flag_x < dest_x)&&(flag_y < dest_y)){
      field[flag_x][flag_y] = 1;
    }
  }
}

function draw_map(size_x, size_y, field) {
	var road = new Image();  // Создание нового объекта изображения
	var bush = new Image();  // Создание нового объекта изображения
	var grass = new Image();  // Создание нового объекта изображения
  bush.src = 'bush.png';
  road.src = 'road.png';
  grass.src = 'grass.png';
  road.onload = function() { 
    grass.onload = function() { 
      var example = document.getElementById("game"), ctx = example.getContext('2d');
      ctx.fillRect(0, 0, example.width, example.height);
      for (xx=0;xx<size_x;xx++) {
      	for (xy = 0;xy<size_y;xy++) {
        	if(field[xx][xy]==0){
          	var landscape = Math.round(Math.random()+Math.random()+1);
          	if ((landscape == 1) || (landscape == 2)) {
          		ctx.drawImage(grass, grid_size*xx, grid_size*xy,grid_size,grid_size);
        		} else if (landscape == 3){
          		ctx.drawImage(bush, grid_size*xx, grid_size*xy,grid_size,grid_size);
        		}
        	}
        	if(field[xx][xy]==1){
        		ctx.drawImage(road, grid_size*xx, grid_size*xy,grid_size,grid_size);
        	}
        }
      }
    }
  }
}
